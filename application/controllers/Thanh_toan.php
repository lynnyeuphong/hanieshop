<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Thanh_toan extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		
		// load the url library
        $this->load->helper('url');
        
        // kết nối đến database 
		$this->load->database();
	}

	public function index()
	{

		$this->load->view('admin/v_thanh_toan.php');
    }
    
 
}
