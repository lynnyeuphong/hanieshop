<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class San_pham extends CI_Controller {
	function __construct()
	{
		parent::__construct();

		// Load thư viện URL
		$this->load->helper('url');

		// Kết nối đến CSDL
		$this->load->database();

		// Load thư viện session
		$this->load->library('session');

		if ($this->session->userdata('email')=="") {
			redirect(base_url()."/admin/dang_nhap.php");
		}
	}
	
	public function index()
	{
		$this->load->view('admin/v_san_pham.php');
	}
}