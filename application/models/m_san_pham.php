<?php 
	class m_san_pham extends CI_Model {
		// Mục đích lấy tin tức theo loại tin
		public function lay_san_pham_theo_loai_san_pham($loai_san_pham_id)
        {
			// Viết câu lệnh truy vấn SQL lấy các tin tức sự kiện (có mã loai_tin_id)
			$query = $this->db->query("
				SELECT * 
				FROM tbl_san_pham
				WHERE loai_san_pham_id=".$loai_san_pham_id."
			");

			// Trả kết quả truy vấn dữ liệu
            return $query->result();
        }

        // Mục đích Lấy tin tức theo ID
		public function lay_san_pham_theo_ID($id)
        {
			// Viết câu lệnh truy vấn SQL lấy các tin tức sự kiện (có mã loai_tin_id)
			$query = $this->db->query("
				SELECT * 
				FROM tbl_san_pham
				WHERE id=".$id
			);

			// Trả kết quả truy vấn dữ liệu
	        return  $query->row();
        }

		public function them_moi_san_pham()
        {
			// Dữ liệu thu được từ FORM nhập dữ liệu
			$loai_san_pham_id = $_POST['txtLoaiSanPham'];
			$so_luong = $_POST['txtSoLuong'];
			$ten_san_pham = $_POST['txtTenSanPham'];
			$mo_ta = $_POST['txtMoTa'];
			$ghi_chu = $_POST['txtGhiChu'];

			// Xử lý đoạn UPLOAD ảnh minh họa
			if (!empty($_FILES['txtAnhMinhHoa']['name'])) {
		        $config['upload_path'] = 'assets\images';
		        $config['allowed_types'] = 'jpg|jpeg|png|gif';
		        $config['file_name'] = $_FILES['txtAnhMinhHoa']['name'];

		        $this->load->library('upload', $config);
		        $this->upload->initialize($config);

		        if ($this->upload->do_upload('txtAnhMinhHoa')) {
		          $uploadData = $this->upload->data();
		          $data["image"] = $uploadData['file_name'];
		        } else{
		          $data["image"] = '';
		        }
		      } else {
		        $data["image"] = '';
		      }

			// Đẩy dữ liệu này vào CSDL
			$data = array(
				'loai_san_pham_id' => $loai_tin_id,
				'so_luong' => $so_luong,
				'anh_minh_hoa' => $data["image"],
				'mo_ta' => $mo_ta,
				'ghi_chu' => $ghi_chu
			);

			// Thực hiện chèn dữ liệu vào bảng TIN TỨC
			$this->db->insert('tbl_san_pham', $data);
        }

		public function sua_san_pham()
        {
        	// Dữ liệu thu được từ FORM nhập dữ liệu
			$id = $_POST['txtID'];
			$loai_san_pham_id = $_POST['txtLoaiSanPham'];
			$so_luong = $_POST['txtSoLuong'];
			$mo_ta = $_POST['txtMoTa'];
			$ghi_chu = $_POST['txtGhiChu'];
			$ten_san_pham = $_POST['txtTenSanPham'];

			// Xử lý đoạn UPLOAD ảnh minh họa
			if (!empty($_FILES['txtAnhMinhHoa']['name'])) {
		        $config['upload_path'] = 'assets\images';
		        $config['allowed_types'] = 'jpg|jpeg|png|gif';
		        $config['file_name'] = $_FILES['txtAnhMinhHoa']['name'];

		        $this->load->library('upload', $config);
		        $this->upload->initialize($config);

		        if ($this->upload->do_upload('txtAnhMinhHoa')) {
		          $uploadData = $this->upload->data();
		          $data["image"] = $uploadData['file_name'];
		        } else{
		          $data["image"] = '';
		        }
		      } else {
		        $data["image"] = '';
		      }

			// Đẩy dữ liệu này vào CSDL

		    if($data["image"] == '')
		    {
				$data = array(
					'loai_san_pham_id' => $loai_san_pham_id,
					'ten_san_pham' => $ten_san_pham,
					'mo_ta' => $mo_ta,
					'so_luong' => $so_luong,
					'ghi_chu' => $ghi_chu
				);
			} else {
				$data = array(
					'loai_san_pham_id' => $loai_san_pham_id,
					'ten_san_pham' => $ten_san_pham,
					'anh_minh_hoa' => $data["image"],
					'mo_ta' => $mo_ta,
					'so_luong' => $so_luong,
					'ghi_chu' => $ghi_chu,
				);
			}

			// Thực hiện cập nhật dữ liệu vào bảng TIN TỨC
			$this->db->where('id', $id);
			$this->db->update('tbl_san_pham', $data);
        }

		public function xoa_san_pham($id)
        {
			// Thực hiện việc xóa dữ liệu
			$this->db->where('id', $id);
			$this->db->delete('tbl_san_pham');
        }

	}
;?>