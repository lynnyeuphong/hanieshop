<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Site Metas -->
    <title>Hanie Shop - Nơi buôn Bán,Ký Gửi Hàng Hóa</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="../images/apple-touch-icon.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <!-- Site CSS -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="assets/css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="assets/css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Start Main Top -->
     
    <div  class="main-top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="text-slid-box">
                        <div id="offer-box" class="carouselTicker">
                            <ul class="offer-box">
                                <li>
                                    <i class="fab fa-opencart"></i> Giảm 10% - Mua ngay nào!
                                </li>
                                <li>
                                    <i class="fab fa-opencart"></i> Giảm 50% - 80% cho sản phẩm thời trang mùa hè
                                </li>
                                <li>
                                    <i class="fab fa-opencart"></i> Giảm 20% khi nhập mã giảm giá: OFFT20
                                </li>
                                <li>
                                    <i class="fab fa-opencart"></i> Giảm 50% - Mua ngay đi!
                                </li>
                                <li>
                                    <i class="fab fa-opencart"></i> Giảm 10% - Mua ngay nào!
                                </li>
                                <li>
                                    <i class="fab fa-opencart"></i> Giảm 50% - 80% cho thời trang mùa hè
                                </li>
                                <li>
                                    <i class="fab fa-opencart"></i> Giảm 20% khi nhập mã giảm giá: OFFT20
                                </li>
                                <li>
                                    <i class="fab fa-opencart"></i> Giảm 50% - Mua ngay đi!
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="custom-select-box">
                        <select id="basic" class="selectpicker show-tick form-control" data-placeholder="$ USD">
                        <option>¥ VND</option>
                        <option>$ USD</option>
                        <option>€ RMB</option>
                    </select>
                    </div>
                    <div class="right-phone-box">
                        <p>Gọi cho chúng tôi :- <a href="#"> +84-975 330 198</a></p>
                    </div>
                    <div class="our-link">
                        <ul>
                            <li><a href="#">Tài khoản </a></li>
                            <li><a href="#">Địa điểm </a></li>
                            <li><a href="#">Liên hệ </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Main Top -->

    <!-- Start Main Top -->
    <header class="main-header">
        <!-- Start Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light navbar-default bootsnav">
            <div class="container">
                <!-- Start Header Navigation -->
                <div class="navbar-header">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                    <a class="navbar-brand" href="index.html"><img src="assets/images/logo.png" class="logo" alt=""></a>
                </div>
                <!-- End Header Navigation -->

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-menu">
                    <ul class="nav navbar-nav ml-auto" data-in="fadeInDown" data-out="fadeOutUp">
                        <li class="nav-item active"><a class="nav-link" href="trang_chu.php">Trang chủ</a></li>
                        <li class="nav-item"><a class="nav-link" href="gioi_thieu.php">Giới thiệu</a></li>
                        <li class="dropdown megamenu-fw">
                            <a href="#" class="nav-link dropdown-toggle arrow" data-toggle="dropdown">Thể loại</a>
                            <ul class="dropdown-menu megamenu-content" role="menu">
                                <li>
                                    <div class="row">
                                        <div class="col-menu col-md-3">
                                            <h6 class="title">Mũ và áo</h6>
                                            <div class="content">
                                                <ul class="menu-col">
                                                    <li><a href="san_pham.php">Áo khoác</a></li>
                                                    <li><a href="san_pham.php">Áo sơ mi</a></li>
                                                    <li><a href="san_pham.php">Áo len</a></li>
                                                    <li><a href="san_pham.php">Áo thun</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- end col-3 -->
                                        <div class="col-menu col-md-3">
                                            <h6 class="title">Quần,váy và phụ kiện</h6>
                                            <div class="content">
                                                <ul class="menu-col">
                                                    <li><a href="san_pham.php">Đồ bơi</a></li>
                                                    <li><a href="san_pham.php">Váy</a></li>
                                                    <li><a href="san_pham.php">Quần Jean</a></li>
                                                    <li><a href="san_pham.php">Quần</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- end col-3 -->
                                        <div class="col-menu col-md-3">
                                            <h6 class="title">Quần áo</h6>
                                            <div class="content">
                                                <ul class="menu-col">
                                                    <li><a href="san_pham.php">Thời trang đang hot</a></li>
                                                    <li><a href="san_pham.php">Váy dự tiệc</a></li>
                                                    <li><a href="san_pham.php">Quần</a></li>
                                                    <li><a href="san_pham.php">Thời trang Ấn Độ</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-menu col-md-3">
                                            <h6 class="title">Phụ kiện</h6>
                                            <div class="content">
                                                <ul class="menu-col">
                                                    <li><a href="san_pham.php">Túi</a></li>
                                                    <li><a href="san_pham.php">Kính râm</a></li>
                                                    <li><a href="san_pham.php">Nước hoa</a></li>
                                                    <li><a href="san_pham.php">Ví tiền</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- end col-3 -->
                                    </div>
                                    <!-- end row -->
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown active">
                            <a href="#" class="nav-link dropdown-toggle arrow" data-toggle="dropdown">Sản phẩm </a>
                            <ul class="dropdown-menu">
                                <li><a href="cart.html">Giỏ hàng</a></li>
                                <li><a href="checkout.html">Thủ tục thanh toán</a></li>
                                <li><a href="dang_nhap.html">Tài khoản </a></li>
                                <li><a href="shop-detail.html">Chi tiết sản phẩm</a></li>
                            </ul>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="service.html">Dịch vụ </a></li>
                        <li class="nav-item"><a class="nav-link" href="phan_hoi.php">Liên hệ </a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->

                <!-- Start Atribute Navigation -->
                <div class="attr-nav">
                    <ul>
                        <li class="search"><a href="#"><i class="fa fa-search"></i></a></li>
                        <li class="side-menu"><a href="#">
                        <i class="fa fa-shopping-bag"></i>
                            <span class="badge">2</span>
                    </a></li>
                    </ul>
                </div>
                <!-- End Atribute Navigation -->
            </div>
            <!-- Start Side Menu -->
            <div class="side">
                <a href="#" class="close-side"><i class="fa fa-times"></i></a>
                <li class="cart-box">
                    <ul class="cart-list">
                        <li>
                            <a href="#" class="photo"><img src="assets/images/Screenshot_1.png" class="cart-thumb" alt="" /></a>
                            <h6><a href="#">Giày chạy bộ Asics Gel-Kayano dành cho nam giới </a></h6>
                            <p>1x - <span class="price">1.400.000 VND</span></p>
                        </li>
                        <li>
                            <a href="#" class="photo"><img src="assets/images/Screenshot_2.png" class="cart-thumb" alt="" /></a>
                            <h6><a href="#">Giày chạy bộ ASICS Men's Weldon X Black/Carbon White Ankle-High</a></h6>
                            <p>1x - <span class="price">845.000 VND</span></p>
                        </li>
                        <li class="total">
                            <a href="#" class="btn btn-default hvr-hover btn-cart">Xem giỏ hàng</a>
                            <span class="float-right"><strong>Total</strong>: 2.245.000 VND</span>
                        </li>
                    </ul>
                </li>
            </div>
            <!-- End Side Menu -->
        </nav>
        <!-- End Navigation -->
    </header>
    <!-- End Main Top -->

    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
        </div>
    </div>
    <!-- End Top Search -->

    <!-- Start Slider -->
    <div id="slides-shop" class="cover-slides">
        <ul class="slides-container">
            <li class="text-left">
                <img src="../images/banner-01.jpg" alt="">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="m-b-20"><strong>Chào mừng bạn đến với <br> Hanie Shop </strong></h1>
                            <p class="m-b-40">Nơi trao đổi, buôn bán và ký gửi hàng hóa<br> Uy tín và chất lượng </p>
                           
                        </div>
                    </div>
                </div>
            </li>
            <li class="text-center">
                <img src="../images/banner-02.jpg" alt="">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="m-b-20"><strong>Chào mừng bạn đến với <br> Hanie Shop </strong></h1>
                            <p class="m-b-40">Mở cửa hàng ngày từ 9AM tới 10PM<br> Từ 2020 </p>
                            
                        </div>
                    </div>
                </div>
            </li>
            <li class="text-right">
                <img src="../images/banner-03.jpg" alt="">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="m-b-20"><strong>Chào mừng bạn đến với <br> Hanie Shop </strong></h1>
                            <p class="m-b-40">Trao đổi, buôn bán và ký gửi hàng hóa<br> Từ 2020</p>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <div class="slides-navigation">
            <a href="#" class="next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
            <a href="#" class="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
        </div>
    </div>
    <!-- End Slider -->

    <!-- Start Categories  -->
    <div class="categories-shop">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="shop-cat-box">
                        <img class="img-fluid" src="assets/images/Screenshot_5.png" alt="" />
                        <a class="btn hvr-hover" href="#">Áo thun</a>
                    </div>
                    <div class="shop-cat-box">
                        <img class="img-fluid" src="assets/images/Screenshot_7.png" alt="" />
                        <a class="btn hvr-hover" href="#">Áo sơ mi</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="shop-cat-box">
                        <img class="img-fluid" src="assets/images/Screenshot_6.png" alt="" />
                        <a class="btn hvr-hover" href="#">Ví tiền</a>
                    </div>
                    <div class="shop-cat-box">
                        <img class="img-fluid" src="assets/images/Screenshot_4.png" alt="" />
                        <a class="btn hvr-hover" href="#">Túi</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="shop-cat-box">
                        <img class="img-fluid" src="assets/images/Screenshot_1.png" alt="" />
                        <a class="btn hvr-hover" href="#">Giày dép</a>
                    </div>
                    <div class="shop-cat-box">
                        <img class="img-fluid" src="assets/images/Screenshot_8.png" alt="" />
                        <a class="btn hvr-hover" href="#">Phụ kiện</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Categories -->

    <!-- Start Products  -->
    <div class="products-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title-all text-center">
                        <h1>Sản phẩm nổi bật</h1>
                        <p>Giới thiệu sản phẩm</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="special-menu text-center">
                        <div class="button-group filter-button-group">
                            <button class="active" data-filter="*">Tất cả</button>
                            <button data-filter=".top-featured">Nổi bật hàng đầu</button>
                            <button data-filter=".best-seller">Bán chạy nhất</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row special-list">
                <div class="col-lg-3 col-md-6 special-grid best-seller">
                    <div class="products-single fix">
                        <div class="box-img-hover">
                            <div class="type-lb">
                                <p class="sale">Giảm giá</p>
                            </div>
                            <img src="assets/images/Screenshot_1.png" class="img-fluid" alt="Image">
                            <div class="mask-icon">
                                <ul>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fas fa-sync-alt"></i></a></li>
                                    
                                </ul>
                                <a class="cart" href="#">Thêm vào giỏ hàng</a>
                            </div>
                        </div>
                        <div class="why-text">
                            <h4>Giày chạy bộ Asics Gel-Kayano dành cho nam giới</h4>
                            <h5> 1.400.000 VND</h5>
                            <h6>Tình trạng : New 85% </h6>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 special-grid top-featured">
                    <div class="products-single fix">
                        <div class="box-img-hover">
                            <div class="type-lb">
                                <p class="new">Mới về</p>
                            </div>
                            <img src="assets/images/Screenshot_2.png" class="img-fluid" alt="Image">
                            <div class="mask-icon">
                                <ul>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fas fa-sync-alt"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                </ul>
                                <a class="cart" href="#">Thêm vào giỏ hàng</a>
                            </div>
                        </div>
                        <div class="why-text">
                            <h4>Giày chạy bộ ASICS Men's Weldon X Black/Carbon White Ankle-High </h4>
                            <h5> 845.000 VND</h5>
                            <h6>Tình trạng : New 100% </h6>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 special-grid top-featured">
                    <div class="products-single fix">
                        <div class="box-img-hover">
                            <div class="type-lb">
                                <p class="sale">Giảm giá</p>
                            </div>
                            <img src="assets/images/Screenshot_3.png" class="img-fluid" alt="Image">
                            <div class="mask-icon">
                                <ul>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fas fa-sync-alt"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                </ul>
                                <a class="cart" href="#">Thêm vào giỏ hàng</a>
                            </div>
                        </div>
                        <div class="why-text">
                            <h4>Giayf Balenciaga Triple S-DRAGON  </h4>
                            <h5> 1.250.000 VND</h5>
                            <h6>Tình trạng : New 95% </h6>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 special-grid best-seller">
                    <div class="products-single fix">
                        <div class="box-img-hover">
                            <div class="type-lb">
                                <p class="sale">Giảm giá</p>
                            </div>
                            <img src="assets/images/Screenshot_4.png" class="img-fluid" alt="Image">
                            <div class="mask-icon">
                                <ul>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fas fa-sync-alt"></i></a></li>
                                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                </ul>
                                <a class="cart" href="#">Thêm vào giỏ hàng</a>
                            </div>
                        </div>
                        <div class="why-text">
                            <h4>Túi trống tập gym 4ATHLTS cỡ vừa</h4>
                            <h5> 1.000.000 VND</h5>
                            <h6>Tình trạng : New 90% </h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Products  -->

    <!-- Start Blog  -->
    <div class="latest-blog">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title-all text-center">
                        <h1>Hanie's Blog</h1>
                        <p>Góc nhỏ của Hanie Hanie </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-4 col-xl-4">
                    <div class="blog-box">
                        <div class="blog-img">
                            <img class="img-fluid" src="assets/images/phoi_do.jpg" alt="" />
                        </div>
                        <div class="blog-content">
                            <div class="title-blog">
                                <h3>Loạt công thức phối đồ nhanh gọn, nàng chuẩn bị vài phút là đã xinh xẻo xuống phố dịp lễ ^^</h3>
                                <p>Dịp nghỉ lễ là khoảng thời gian các nàng có thể tha hồ xúng xính váy áo xuống phố dạo chơi. Chắc chắn chị em nào cũng muốn tìm cho mình một set đồ nhanh gọn nhưng vẫn phải chuẩn xinh, rạng rỡ nhất trong những ngày này.</p>
                            </div>
                            <ul class="option-blog">
                                <li><a href="#" data-toggle="tooltip" data-placement="right" title="Likes"><i class="far fa-heart"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="right" title="Views"><i class="fas fa-eye"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="right" title="Comments"><i class="far fa-comments"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-xl-4">
                    <div class="blog-box">
                        <div class="blog-img">
                            <img class="img-fluid" src="assets/images/2-1054.jpg" alt="" />
                        </div>
                        <div class="blog-content">
                            <div class="title-blog">
                                <h3>Tips hô biến chân ngắn thành chân dài chỉ trong 5 phút</h3>
                                <p>Những cô nàng chân ngắn hoàn toàn có thể khắc phục khuyết điểm của mình nhờ những cách chọn trang phục, và biết tạo điểm nhấn đúng chỗ. Bạn hãy cùng tham khảo 6 kiểu trang phục giúp cho đôi chân như được kéo dài ra, tăng thêm vài cm chiều cao dưới đây nhé!</p>
                            </div>
                            <ul class="option-blog">
                                <li><a href="#" data-toggle="tooltip" data-placement="right" title="Likes"><i class="far fa-heart"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="right" title="Views"><i class="fas fa-eye"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="right" title="Comments"><i class="far fa-comments"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-xl-4">
                    <div class="blog-box">
                        <div class="blog-img">
                            <img class="img-fluid" src="assets/images/eobanhmy.jpg" alt="" />
                        </div>
                        <div class="blog-content">
                            <div class="title-blog">
                                <h3>5 trang phục giúp biến eo ngấn mỡ hoá thon gọn, nàng diện ở nhà vẫn không lo rườm rà</h3>
                                <p>Dù sở hữu “vòng eo bánh mì”, các cô gái vẫn có thể khắc phục nhược điểm này bằng quần áo. Khi biết khéo chọn trang phục, chị em kể cả khi ở nhà hoàn toàn vẫn có thể hô biến vòng 2 của mình lập tức trở nên thon gọn.</p>
                            </div>
                            <ul class="option-blog">
                                <li><a href="#" data-toggle="tooltip" data-placement="right" title="Likes"><i class="far fa-heart"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="right" title="Views"><i class="fas fa-eye"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="right" title="Comments"><i class="far fa-comments"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Blog  -->


    <!-- Start Instagram Feed  -->
    <div class="instagram-box">
        <div class="main-instagram owl-carousel owl-theme">
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-01.jpg" alt="" />
                    <div class="hov-in">
                        <a href="#"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-02.jpg" alt="" />
                    <div class="hov-in">
                        <a href="#"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-03.jpg" alt="" />
                    <div class="hov-in">
                        <a href="#"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-04.jpg" alt="" />
                    <div class="hov-in">
                        <a href="#"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-05.jpg" alt="" />
                    <div class="hov-in">
                        <a href="#"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-06.jpg" alt="" />
                    <div class="hov-in">
                        <a href="#"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="assets/images/instagram-img-07.jpg" alt="" />
                    <div class="hov-in">
                        <a href="#"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-08.jpg" alt="" />
                    <div class="hov-in">
                        <a href="#"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-09.jpg" alt="" />
                    <div class="hov-in">
                        <a href="#"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-05.jpg" alt="" />
                    <div class="hov-in">
                        <a href="#"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Instagram Feed  -->


    <!-- Start Footer  -->
    <footer>
        <div class="footer-main">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="footer-widget">
                            <h4>Về HanieShop </h4>
                            <p>Nơi trao đổi, buôn bán và ký gửi hàng hiệu với mức giá ưu đãi 
                                </p>
                            <ul>
                                <li><a href="#"><i class="fab fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fab fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fab fa-google-plus" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fab fa-pinterest-p" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fab fa-whatsapp" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="footer-link">
                            <h4>Thông tin</h4>
                            <ul>
                                <li><a href="#">Về chúng tôi</a></li>
                                <li><a href="#">Dịch vụ khách hàng</a></li>
                                <li><a href="#">Sơ đồ trang Web</a></li>
                                <li><a href="#">Điều khiện &amp; Điều kiện</a></li>
                                <li><a href="#">Chính sách bảo mật</a></li>
                                <li><a href="#">Thông tin giao hàng</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="footer-link-contact">
                            <h4>Liên hệ với chúng tôi</h4>
                            <ul>
                                <li>
                                    <p><i class="fas fa-map-marker-alt"></i>Địa chỉ : 12 Chua Boc,Dong Da<br>Hanoi,<br> KS 67213 </p>
                                </li>
                                <li>
                                    <p><i class="fas fa-phone-square"></i>Điện thoại: <a href="tel:+84975330198">+84 975330198</a></p>
                                </li>
                                <li>
                                    <p><i class="fas fa-envelope"></i>Email: <a href="mailto:emyeuanhhangxom@gmail.com">emyeuanhhangxom@gmail.com</a></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer  -->

    <!-- Start copyright  -->
    <div class="footer-copyright">
        <p class="footer-company">Đã đăng kí bản quyền. &copy; 2018 <a href="#">HanieShop</a> Design By :
            <a href="https://html.design/">HTML DESIGN</a></p>
    </div>
    <!-- End copyright  -->

    <a href="#" id="back-to-top" title="Back to top" style="display: none;">&uarr;</a>

    <!-- ALL JS FILES -->
    <script src="assets/js/jquery-3.2.1.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="assets/js/jquery.superslides.min.js"></script>
    <script src="assets/js/bootstrap-select.js"></script>
    <script src="assets/js/inewsticker.js"></script>
    <script src="assets/js/bootsnav.js."></script>
    <script src="assets/js/images-loded.min.js"></script>
    <script src="assets/js/isotope.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/baguetteBox.min.js"></script>
    <script src="assets/js/form-validator.min.js"></script>
    <script src="assets/js/contact-form-script.js"></script>
    <script src="assets/js/custom.js"></script>
</body> 
</html>