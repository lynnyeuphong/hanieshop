-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th5 04, 2020 lúc 01:28 AM
-- Phiên bản máy phục vụ: 10.4.11-MariaDB
-- Phiên bản PHP: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `hanie_shop`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_lien_he`
--

CREATE TABLE `tbl_lien_he` (
  `ho_va_ten` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `loi_nhan` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_lien_he`
--

INSERT INTO `tbl_lien_he` (`ho_va_ten`, `email`, `loi_nhan`) VALUES
('Hoang Ha Nhi', 'emyeuanhhangxom@gmail.com', 'anbccccc'),
('hpang ha ', 'khdkdkkdk@gmail.com', 'nhi xinh');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_nguoi_dung`
--

CREATE TABLE `tbl_nguoi_dung` (
  `Id` int(11) NOT NULL,
  `ho_ten` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mat_khau` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ghi_chu` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_nguoi_dung`
--

INSERT INTO `tbl_nguoi_dung` (`Id`, `ho_ten`, `email`, `mat_khau`, `ghi_chu`) VALUES
(1, 'hoang ha nhi', 'emyeuanhhangxom@gmail.com', '473d4a79bfc536616e5be128585dba7e', ''),
(2, 'robert patison', 'hdhdh@gmail.com', '473d4a79bfc536616e5be128585dba7e', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_san_pham`
--

CREATE TABLE `tbl_san_pham` (
  `id` int(50) NOT NULL,
  `ten_san_pham` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `so_luong` int(50) NOT NULL,
  `mo_ta` text COLLATE utf8_unicode_ci NOT NULL,
  `ghi_chu` text COLLATE utf8_unicode_ci NOT NULL,
  `anh_minh_hoa` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_san_pham`
--

INSERT INTO `tbl_san_pham` (`id`, `ten_san_pham`, `so_luong`, `mo_ta`, `ghi_chu`, `anh_minh_hoa`) VALUES
(1, 'Giày chạy bộ Under Armour Heater Mid TPU Molded Cl', 1, 'Hàng mới,không có hộp: Một mặt hàng hoàn toàn mới, chưa sử dụng và chưa sử dụng (bao gồm cả đồ thủ công) không có trong bao bì gốc hoặc có thể thiếu các vật liệu đóng gói ban đầu (như hộp hoặc túi ban đầu). Các thẻ gốc có thể không được đính kèm. Ví dụ, giày mới (hoàn toàn không có dấu hiệu bị mòn) không còn trong hộp ban đầu của chúng rơi vào danh mục này.', 'Không có hộp đi kèm', 'Screenshot_9.png');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_tin_tuc`
--

CREATE TABLE `tbl_tin_tuc` (
  `Id` int(11) NOT NULL,
  `loai_tin_id` int(11) NOT NULL,
  `tieu_de` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `mo_ta` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `noi_dung` text COLLATE utf8_unicode_ci NOT NULL,
  `anh_minh_hoa` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `so_luot_doc` int(11) NOT NULL,
  `tac_gia` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ngay_dang` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_tin_tuc`
--

INSERT INTO `tbl_tin_tuc` (`Id`, `loai_tin_id`, `tieu_de`, `mo_ta`, `noi_dung`, `anh_minh_hoa`, `so_luot_doc`, `tac_gia`, `ngay_dang`) VALUES
(1, 1, 'Gần 5.000 người chết ở Anh vì nCoV', 'Anh hôm nay báo cáo thêm 621 người chết do nCoV, giảm 12% so với một ngày trước, nâng tổng số ca tử vong toàn quốc lên 4.934.', 'Anh đến nay xét nghiệm được cho 195.524 người, trong đó 47.806 trường hợp dương tính, theo số liệu từ Bộ Y tế nước này.', 'covid_tai_anh.jpg', 0, 'VnExpress.net', '2020-04-05 17:28:41'),
(2, 1, 'Singapore nguy cơ \'vỡ trận\' khi tưởng khống chế được Covid-19', 'Bất chấp các biện pháp phòng chống đại dịch nghiêm ngặt, Singapore ghi nhận số ca nhiễm nCoV tăng gấp gần 10 lần chỉ trong một tháng.', 'Vào đầu tháng 3, Singapore ghi nhận hơn 100 ca nhiễm nCoV và trở thành niềm cảm hứng trong trận chiến chống đại dịch toàn cầu của nhiều quốc gia trên thế giới. Theo dõi liên lạc chủ động, kiểm dịch nghiêm ngặt, hạn chế đi lại và xét nghiệm quy mô lớn giúp Singapore làm được điều này. \r\n\r\nNhiều quốc gia khi đó ghen tị trước quốc đảo nhỏ bé khi Singapore thực hiện hàng loạt biện pháp hiệu quả để giữ mức độ lây nhiễm thấp, trong lúc vẫn cho trường học và trung tâm thương mại hoạt động bình thường. Số ca nhiễm mới mỗi ngày ở Singapore hồi tháng 2 chỉ ở một con số.\r\n\r\nThế nhưng đến đầu tháng 4, số ca nhiễm tại Singapore chạm mốc 1.000 và bức tranh không còn màu hồng. Ngày 1/4, Singapore xác nhận thêm 74 ca nhiễm nCoV mới, một ngày sau nước này thêm 49 ca nhiễm và một người chết. Giới chuyên gia nhận định đất nước 5,7 triệu dân đang hứng đợt bùng phát Covid-19 thứ hai.', 'nhan_vien_yte.jpg', 0, 'VnExpress.net', '2020-04-05 17:29:16'),
(3, 2, 'Người Việt bất ngờ trước lệnh phong toả ở Ấn Độ ', 'Tối 24/3, Linh, sống tại New Delhi, ngỡ ngàng khi nghe Thủ tướng Ấn Độ tuyên bố phong toả toàn quốc để chặn Covid-19, áp dụng luôn cho ngày hôm sau. ', 'Thông báo mới của Thủ tướng Narendra Modi được phát trên truyền hình quốc gia tối 24/3. Ông cho biết lệnh phong toả có hiệu lực trong 21 ngày nhằm \"cứu Ấn Độ\" trước Covid-19, cảnh báo nếu không xử lý tốt, Ấn Độ có thể tụt hậu 21 năm.   \r\n\r\n\"Tôi bất ngờ vì chưa chuẩn bị tâm lý cho lệnh cấm dài hơn, khi Ấn Độ vừa kết thúc lệnh giới nghiêm từ ngày 22/3\", Nguyễn Huỳnh Khánh Linh nói. Lệnh giới nghiêm kéo dài 14 tiếng được cho là thử nghiệm quan trọng của Ấn Độ về khả năng chống lại đại dịch. \r\n\r\nẤn Độ khi đó ghi nhận hơn 500 ca nhiễm nCoV và 10 người chết, thấp hơn nhiều nước khác. Tuy nhiên, các chuyên gia y tế cho biết số bệnh nhân tại nước này đang gia tăng với tốc độ tương đương giai đoạn đầu của đợt bùng phát dịch ở các quốc gia khác. Tình trạng này sau đó thường dẫn tới sự gia tăng ca nhiễm nCoV theo cấp số nhân.   \r\n\r\nSau giây phút cảm thấy \"trở tay không kịp\", Linh quyết định đặt mua các loại thực phẩm tươi vì đã đủ đồ khô dành cho 7 người trong gia đình. Tuy nhiên, tất cả các nhà cung cấp đều báo không thể giao hàng vì không có giấy phép đi lại. Đến ngày 31/3, sau một tuần ở trong nhà, Linh đi mua thêm thực phẩm và phải giải trình khá lâu với cảnh sát về lý do ra đường. Khi chính phủ bỏ quy định giấy phép đi lại, các kênh bán hàng trực tuyến lại thiếu người giao hàng. Đến đầu tháng 4, nhiều nhà cung cấp thông báo chỉ nhận đơn khi khách trả tiền trước. \r\n\r\nTình trạng quá tải đơn hàng online cũng diễn ra tại Gujarat, bang phía tây Ấn Độ, cách New Delhi hơn 900 km, theo Nguyễn Thanh Tâm. Các thành viên trong gia đình cô đi chợ để mua rau quả tươi hai ngày một lần. Mọi người đều mang khẩu trang, rửa tay để tránh nCoV. Gia đình chồng Tâm, là người Ấn, ăn chay nên cô nói vui rằng mình \"không bị áp lực nhiều về ăn uống\". Người dân ở Gujarat đều trữ đồ khô theo thói quen nên không có hiện tượng đổ xô mua sắm.    \r\n\r\nTại bang Himachal Pradesh ở đông bắc Ấn Độ, nằm trên dãy Himalaya, nơi Trần Tường Thụy đang sinh sống, chính quyền quy định người dân chỉ được đi ra đường trong thời gian 8-11h sáng để mua đồ. Cảnh sát không cho phép dùng phương tiện cá nhân để di chuyển, nhiều người phải đi bộ xa để mua sắm. Sau khi người dân kiến nghị, nhà chức trách cho phép họ gom chung để đặt hàng và chuyển tiền online, nhằm hạn chế tiếp xúc. Thuỵ chỉ mua đồ đủ dùng trong vài ngày, không tích trữ nhiều, vì các cửa hàng vẫn mở bán theo khung giờ quy định.', 'nguoi_an_do.jpg', 0, 'VnExpress.net', '2020-04-05 17:27:20'),
(4, 2, 'Toàn cầu giành giật khẩu trang ', 'Từ giữ lô hàng cho đến \"hớt tay trên\" ngay tại đường băng, săn lùng khẩu trang đã trở thành cuộc cạnh tranh không còn luật chơi công bằng.', 'Nhiều quốc gia, đặc biệt là các vùng dịch lớn như Mỹ và châu Âu, lâm vào tình trạng thiếu khẩu trang vì hầu hết không thể sản xuất hàng triệu khẩu trang mà nhân viên y tế cần mỗi ngày. Vì vậy, gần như tất cả đang trông đợi vào những lô hàng từ Trung Quốc và các nhà sản xuất khác ở châu Á. Một số nước sẵn sàng làm bất cứ điều gì để có được mặt hàng nhiều bên đang thèm muốn.\r\n\r\n\"Thị trường mua sắm vật tư y tế đối phó Covid-19 đang trở nên méo mó, các hình thức cạnh tranh và sự minh bạch trước đây không còn nữa\", Christopher Yukins, giáo sư luật tại Đại học George Washington, nói.', 'gianh_giat_khau_trang.jpg', 0, 'VnExpress', '2020-04-05 17:27:20');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `tbl_nguoi_dung`
--
ALTER TABLE `tbl_nguoi_dung`
  ADD PRIMARY KEY (`Id`);

--
-- Chỉ mục cho bảng `tbl_san_pham`
--
ALTER TABLE `tbl_san_pham`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_tin_tuc`
--
ALTER TABLE `tbl_tin_tuc`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `tbl_nguoi_dung`
--
ALTER TABLE `tbl_nguoi_dung`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `tbl_san_pham`
--
ALTER TABLE `tbl_san_pham`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `tbl_tin_tuc`
--
ALTER TABLE `tbl_tin_tuc`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
